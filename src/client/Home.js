import React, { Component } from 'react';
import Keycloak from 'keycloak-js';
import keycloakConf from '../../public/keycloak.json';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keycloak: null,
      authenticated: false,
      name: '',
      email: '',
      id: ''
    };
  }

  componentDidMount() {
    const keycloak = Keycloak(keycloakConf);
    keycloak.init({ onLoad: 'login-required' })
      .then((authenticated) => {
        keycloak.loadUserInfo()
          .then((userInfo) => {
            this.setState({
              keycloak,
              authenticated,
              name: userInfo.name,
              email: userInfo.email,
              id: userInfo.sub
            });
          });
      });
  }

  render() {
    if (this.state.keycloak) {
      if (this.state.authenticated) {
        return (
          <div className="UserInfo">
              User is logged in
            <p>
              Name:
              {this.state.name}
            </p>
            <p>
              Email:
              {this.state.email}
            </p>
            <p>
              ID:
              {this.state.id}
            </p>
          </div>
        );
      }
    }

    return (<div>Unauthenticated</div>);
  }
}

export default Home;
